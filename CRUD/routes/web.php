<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastControlller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'Home']);
Route::get('/register', [AuthController::class, 'registerr']);
Route::post('/welcome', [AuthController::class, 'welcomee']);

// testing tampilan
Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/table', function(){
    return view('partials.table');
});

Route::get('/data-table', function(){
    return view('partials.data-table');
});

// CRUD
// Membuat data pemain atau create cast
Route::get('/cast/create', [CastControlller::class, 'create']);

// route untuk menyimpan inputkan ke db table cast
Route::POST('/cast', [CastControlller::class, 'store']);