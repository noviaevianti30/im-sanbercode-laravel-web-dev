@extends('layout.master')
@section('title')
    Halaman Tambah Pemain Film
@endsection

@section('content')
<form method="POST" action="/cast">
  @csrf
  <div class="form-group">
    <label >Cast Name</label>
    <input type="text" name="nama" class="form-control" placeholder="Enter name">
  </div>
  @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label >Age</label>
    <input type="int" name="Age" class="form-control" placeholder="Enter age">
  </div>
  @error('Age')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label >Bio :</label>
    <textarea name="Bio" class="form-control" cols="30" rows="10"></textarea>
  </div>
  @error('Bio')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection