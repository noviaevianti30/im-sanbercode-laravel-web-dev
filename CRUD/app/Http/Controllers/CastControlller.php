<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastControlller extends Controller
{
    public function create()
    {
        return view('pemain.tambah');
    }

    public function store(Request $request)
    {
        // validasi data
        $request->validate([
            'Cast Name' => 'required|min5',
            'Age' => 'required|min5',
            'Bio' => 'required|min5',
        ]);

        // masukkan data request ke table cast di database
        DB::table('cast')->insert([
            'Cast Name' => $request['Cast Name'],
            'Age' => $request['Age'],
            'Bio' => $request['Bio'],
        ]);

        // kita lempar ke halaman /cast
        return redirect('/cast');
    }
}
