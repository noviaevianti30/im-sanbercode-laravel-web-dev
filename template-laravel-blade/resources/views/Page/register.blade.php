@extends('layout.master')
@section('title')
<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>
@endsection

@section('content')
<form action="/welcome" method="POST">
    @csrf
    <label>First name:</label><br> 
    <input type="text" name="fname"><br><br>

    <label>Last name:</label><br>
    <input type="text" name="lname"><br><br> 

    <label>Gender:</label><br><br>
    <input type="radio">Male<br>
    <input type="radio">Female<br>
    <input type="radio">Other<br><br>

    <label>Nationality:</label><br><br>
    <select name="nationality">
        <option value="Indonesian">Indonesian</option>
        <option value="Brazil">Brazil</option>
        <option value="albanian">Albanian</option>
        <option value="algerian">Algerian</option>
        <option value="american">American</option>
        <option value="andorran">Andorran</option>
        <option value="angolan">Angolan</option>
        <option value="antiguans">Antiguans</option>
        </select><br><br>

    <label >Language Spoken:</label><br>
    <input type="checkbox" name="Language Spoken">Bahasa Indonesia<br>
    <input type="checkbox" name="Language Spoken">English<br> 
    <input type="checkbox" name="Language Spoken">Other<br><br>
    
    <label >Bio:</label><br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea>
        <br><br>

    <input type="submit" value="Sign Up">
@endsection